﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    // this script simply moves an object around the screen based on player input from keyboard, gamepad, or on-screen touch controls as 
    // managed by an InputManager script

    InputManager inputManager;

    [SerializeField] float playerSpeed = 5f;

    private void Awake()
    {
        inputManager = GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update ()
    {
        transform.Translate(inputManager.CurrentInput * Time.deltaTime * playerSpeed);
	}
}
