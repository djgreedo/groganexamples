﻿using UnityEngine;

public class MoveToClickInput : MonoBehaviour
{
    [SerializeField] Transform target;

    float speed = 6f;

    Vector2 targetPos;

    private void Start()
    {
        targetPos = transform.position;
    }

    void Update ()
    {
        // use GetMouseButton for continuous following or GetMouseButtonDown to only move to specific touch points
		if(Input.GetMouseButton(0))
        {
            targetPos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            target.position = targetPos;
        }

        if((Vector2)transform.position != targetPos)
        {
            transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        }
	}
}
